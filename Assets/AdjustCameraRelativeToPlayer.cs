﻿using UnityEngine;

public class AdjustCameraRelativeToPlayer : MonoBehaviour {

	public Transform Player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var position = transform.InverseTransformPoint(Player.position);
		if (position.y > 0f)
		{
			var myPosition = transform.position;
			myPosition.y = Player.position.y;
			transform.position = myPosition;
		}
	}
}
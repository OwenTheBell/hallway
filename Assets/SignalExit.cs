﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Events.instance.Raise(new ExitEvent());
		Destroy(gameObject);
	}
}

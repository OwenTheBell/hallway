﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraOnCollision : MonoBehaviour
{

	public CircleCollider2D PlayerCollider;

	private void OnTriggerStay2D(Collider2D collision)
	{
		var position = Camera.main.transform.position;
		position.y = PlayerCollider.transform.position.y + PlayerCollider.radius;
		Camera.main.transform.position = position;
	}
}

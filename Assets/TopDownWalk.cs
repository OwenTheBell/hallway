﻿using UnityEngine;

public class TopDownWalk : MonoBehaviour {

	public float Speed;
	public AudioClip[] Steps;
	public float StepEvery;

	private Rigidbody2D _Body;
	private AudioSource _Source;
	private float _ToStep = 0f;
	private int _StepIndex = 0;

	private void Awake()
	{
		_Body = GetComponent<Rigidbody2D>();
		_Source = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		_Body.MovePosition((Vector2)transform.position + move * Speed * Time.deltaTime);
		if (move.sqrMagnitude > 0f && Speed > 0f)
		{
			_ToStep -= Speed * Time.deltaTime;
			if (_ToStep <= 0f)
			{
				_ToStep = StepEvery;
				_Source.clip = Steps[_StepIndex];
				_Source.Play();
				if (++_StepIndex > Steps.Length - 1)
				{
					_StepIndex = 0;
				}
			}
		}
		else
		{
			_ToStep = 0f;
			_StepIndex = 0;
		}
	}
}

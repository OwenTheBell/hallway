﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

	public GameObject Player;
	public GameObject CurrentChunk;
	public float GameDistance;
	[Range(0f, 1f)]
	public float LossWithClose;
	public int Min;
	public int Max;
	public int FirstDoor;
	[Range(0f, 1f)]
	public float MinimumScale;
	public float MinimumSpeed;

	public GameObject ChunkPrefab;

	public Vector2 DoorApproachRange;
	public AudioClip[] DoorClips;
	public AudioSource OpenSource;
	public AudioSource CloseSource;

	public GameObject PanelObject;
	public GameObject Title;
	public GameObject Creator;

	private Vector2 _StartingPosition;
	private float _StartingSpeed;
	private Door _CurrentDoor;

	private bool _DoorMode = false;
	private bool _ReturningToHall = false;
	private float _PlayerSpeed;
	private float _PlayerScale = 1f;
	private List<int> _NextDoorGaps;
	private Vector2 _ReachDoorRange;
	private bool _GameOver = false;


	private void Awake()
	{
		_NextDoorGaps = new List<int>();
		Events.instance.AddListener<ExitEvent>(OnExitEvent);
		Events.instance.AddListener<EnterEvent>(OnEnterEvent);
		Events.instance.AddListener<ApproachingDoorEvent>(OnApproachingDoorEvent);
		PanelObject.SetActive(false);
		Title.SetActive(false);
		Creator.SetActive(false);
	}

	void Start()
	{
		_StartingPosition = Player.transform.position;
		_StartingSpeed = Player.GetComponent<TopDownWalk>().Speed;
		_CurrentDoor = CurrentChunk.GetComponentInChildren<Door>();
		ResetDoorGaps();
		_NextDoorGaps.Insert(0, FirstDoor);
		Cursor.visible = false;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		if (_DoorMode)
		{
			if (Input.GetAxis("Horizontal") * _CurrentDoor.transform.position.x > 0f)
			{
				var doorX = _CurrentDoor.transform.position.x;
				doorX -= 1 * (doorX / Mathf.Abs(doorX));
				var playerX = Player.transform.position.x - _ReachDoorRange.x;

				var maximumPercent = Mathf.Lerp(DoorApproachRange.y, DoorApproachRange.x, _PlayerScale);
				var percentToDoor = Mathf.Abs(playerX) / Mathf.Abs(_CurrentDoor.transform.position.x);
				//Debug.Log("percent distance to door: " + percentToDoor);

				var p = Mathf.Clamp01(percentToDoor / maximumPercent);
				var easeIn = Easing.EaseIn(1f - p, 2);
				if (easeIn < 0.02f)
				{
					easeIn = 0f;
				}
				Player.GetComponent<TopDownWalk>().Speed = _PlayerSpeed * easeIn;

				var scalePercent = percentToDoor / DoorApproachRange.y;
				var scale = Mathf.Lerp(_PlayerScale, 1f, Easing.EaseInOut(scalePercent, 2));
				Player.transform.localScale = Vector3.one * Easing.EaseOut(scale, 2); 
			}
			else
			{
				StartCoroutine(Backlash());
			}
		}
		else if (!_GameOver)
		{
			var difference = Mathf.Clamp01((Player.transform.position.y - _StartingPosition.y) / GameDistance);
			_PlayerScale = Mathf.Clamp(1f - difference, MinimumScale, 1f);
			if (!_ReturningToHall)
			{
				var reflectedScale = Easing.EaseOut(_PlayerScale, 2);
				Player.transform.localScale = Vector3.one * reflectedScale;
				var speed = Mathf.Clamp(_StartingSpeed * reflectedScale, MinimumSpeed, _StartingSpeed);
				Player.GetComponent<TopDownWalk>().Speed = speed;
			}
		}
	}

	void OnEnterEvent(EnterEvent e)
	{
		var nextChunkPosition = CurrentChunk.transform.position;
		nextChunkPosition.y += Random.Range(6.5f, 8f);
		var newChunk = Instantiate(ChunkPrefab, nextChunkPosition, Quaternion.identity);
		if (Random.value > 0.5f)
		{
			var scale = newChunk.transform.localScale;
			scale.x *= -1f;
			newChunk.transform.localScale = scale;
		}
		CurrentChunk = newChunk;
		_PlayerSpeed = Player.GetComponent<TopDownWalk>().Speed;

		if (--_NextDoorGaps[0] > 0 && _PlayerScale >= LossWithClose)
		{
			return;
		}
		//_NextDoor = Random.Range(Min, Max);
		_NextDoorGaps.RemoveAt(0);
		if (_NextDoorGaps.Count == 0)
		{
			ResetDoorGaps();
		}
		_CurrentDoor.Open();
		OpenSource.Play();
	}

	void OnExitEvent(ExitEvent e)
	{
		if (_CurrentDoor.IsOpen)
		{
			if (_PlayerScale == MinimumScale)
			{
				CloseSource.clip = DoorClips[DoorClips.Length - 1];
			}
			else
			{
				CloseSource.clip = DoorClips[0];
			}
			CloseSource.Play();
		}
		_DoorMode = false;
		_CurrentDoor.Close();
		_CurrentDoor = CurrentChunk.GetComponentInChildren<Door>();

		if (_PlayerScale == MinimumScale)
		{
			StartCoroutine(EndGame());
		}
	}

	void OnApproachingDoorEvent(ApproachingDoorEvent e)
	{
		_DoorMode = true;
		_ReachDoorRange = new Vector2(Player.transform.position.x, _CurrentDoor.transform.position.x);
	}

	IEnumerator Backlash()
	{
		_ReturningToHall = true;
		var direction = _CurrentDoor.transform.position - Player.transform.position;
		direction.y = 0f;
		direction.Normalize();
		direction *= -1;

		var walk = Player.GetComponent<TopDownWalk>();
		walk.enabled = false;

		//Player.GetComponent<Rigidbody2D>().AddForce(direction * 40f, ForceMode2D.Impulse);
		_DoorMode = false;
		_CurrentDoor.Close();

		var clipIndex = 0;
		if (_PlayerScale <= LossWithClose)
		{
			clipIndex = DoorClips.Length - 1;
		}
		else
		{
			var percent = 1f - Mathf.Clamp01(_PlayerScale / (1f - LossWithClose));
			clipIndex = Mathf.FloorToInt(percent * (DoorClips.Length - 1));
		}

		CloseSource.clip = DoorClips[clipIndex];
		CloseSource.Play();
		var duration = 0.1f;
		var time = 0f;
		var playerStart = Player.transform.position;
		var playerEnd = playerStart;
		playerEnd.x = 0f;
		var start = _StartingPosition;
		var target = _StartingPosition + Vector2.down * (GameDistance * LossWithClose);
		var startScale = Player.transform.localScale;
		while (time < duration)
		{
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time / duration, 2);
			_StartingPosition = Vector2.Lerp(start, target, easeInOut);
			var easeIn = Easing.EaseIn(time / duration, 2);
			Player.transform.position = Vector2.Lerp(playerStart, playerEnd, easeIn);
			var scale = Easing.EaseOut(_PlayerScale, 2);
			Player.transform.localScale = Vector3.Lerp(startScale, Vector3.one * scale, easeIn);
			yield return 0;
		}

		if (_PlayerScale <= MinimumScale)
		{
			StartCoroutine(EndGame());
		}

		_ReturningToHall = false;
		walk.enabled = true;
		walk.Speed = _PlayerSpeed;
	}

	void ResetDoorGaps()
	{
		_NextDoorGaps.Clear();
		for (var i = Min; i < Max; i++)
		{
			_NextDoorGaps.Add(i);
		}
		for (var i = 0; i < _NextDoorGaps.Count; i++)
		{
			var j = Random.Range(0, _NextDoorGaps.Count);
			var holding = _NextDoorGaps[j];
			_NextDoorGaps[j] = _NextDoorGaps[i];
			_NextDoorGaps[i] = holding;
		}
	}

	IEnumerator EndGame()
	{
		// Game Over
		Debug.Log("Game Over");
		Player.SetActive(false);
		_GameOver = true;
		yield return new WaitForSeconds(4f);
		PanelObject.SetActive(true);
		Title.SetActive(true);
		yield return new WaitForSeconds(4f);
		Title.SetActive(false);
		Creator.SetActive(true);
		yield return new WaitForSeconds(6f);
		Application.Quit();
	}
}
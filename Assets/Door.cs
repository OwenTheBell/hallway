﻿using UnityEngine;

public class Door : MonoBehaviour
{

	public GameObject Light;
	public GameObject Collider;
	private SpriteRenderer _Renderer;

	public bool IsOpen { get; private set; }

	private void Awake()
	{
		_Renderer = GetComponent<SpriteRenderer>();
		Light.SetActive(false);
		IsOpen = false;
	}

	public void Open()
	{
		Light.SetActive(true);
		_Renderer.enabled = false;
		Collider.SetActive(true);
		IsOpen = true;
	}

	public void Close()
	{
		Light.SetActive(false);
		_Renderer.enabled = true;
		IsOpen = false;
	}
}